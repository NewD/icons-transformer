#!/bin/bash

libSSH="git@gitlab.rcbi.pro:rc/core/ui-library.git"
libDirName="ui-library"
branchName="_update-icons"
pathToIcons="icons"

if [ -d $libDirName ]; then
    rm -Rf $libDirName
fi

echo "Клонирование репозитория $libDirName..."
git clone $libSSH &> /dev/null

echo 'Запуск основного скрипта'

# $1 - u can send flag -a or -A to update all icons!
node src/index.js $1

echo 'Сохранение изменений в гите...'

cd $libDirName
git branch -D $branchName &> /dev/null
git push origin :$branchName &> /dev/null
git checkout -b $branchName &> /dev/null
git add $pathToIcons &> /dev/null
git commit -m 'updated icons' &> /dev/null
git push --set-upstream origin $branchName &> /dev/null

echo -e "Перейти к созданию mr'a? (y/n)"

read answ

if [[ $answ == 'y' || $answ == 'Y' ]]; then
    xdg-open https://gitlab.rcbi.pro/rc/core/ui-library/-/merge_requests/new?merge_request%5Bsource_branch%5D=_update-icons
fi
