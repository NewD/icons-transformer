const { coloredPrefix } = require('../../constants');
const { formatName } = require('../namesFormatters/formatNameHelper');
const { getFilePath } = require('../namesFormatters/getFilePathHelper');
const { downloadFile } = require('./downloadFileHelper');

function downloadIcons(iconsInfo, idsNames) {
    return iconsInfo.map(([iconId, link]) => {
        const rawIconName = idsNames.names.get(iconId);
        const isColoredIcon = rawIconName.startsWith(coloredPrefix);

        // remove coloredPrefix from iconName
        const iconName = isColoredIcon
            ? formatName(rawIconName.split(coloredPrefix)[1])
            : rawIconName;

        const filePath = getFilePath(iconName);
        
        return downloadFile({
            filePath,
            link,
            isColoredIcon,
            iconName
        });
    });
}

module.exports.downloadIcons = downloadIcons;
