const fs = require('fs');
const https = require('https');
const {
    convertSvgToDynamicFillSvg
} = require('../filesFormatters/convertSvgToDynamicFillSvg');

function downloadFile({
    filePath, link, isColoredIcon, iconName
}) {
    return new Promise((resolve, reject) => https.get(link, response => {
        response.setEncoding('utf8');
        let rawData = '';
        response.on('data', chunk => {
            try {
                rawData += chunk;
            } catch (err) {
                reject(err);
            }
        });

        response.on('end', () => {
            try {
                const newIcon = convertSvgToDynamicFillSvg(rawData, iconName, isColoredIcon);
                fs.writeFileSync(filePath, newIcon);
                resolve();
            } catch (err) {
                console.error(err.message);
                reject(err);
            }
        });
    }));
}

module.exports.downloadFile = downloadFile;
