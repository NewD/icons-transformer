const fs = require('fs');
const path = require('path');

function getExistedIcons() {
    const iconsContainerPath = path.resolve('', process.env.ICONS_DIR_PATH);
    const iconExtension = process.env.ICON_EXTENSION;

    const currentDirContent = fs.readdirSync(iconsContainerPath, {
        withFileTypes: true
    });

    return currentDirContent.reduce((acc, item) => {
        if (item.isFile() && item.name.endsWith(iconExtension)) {
            return acc.add(item.name.split(iconExtension)[0]);
        }

        return acc;
    }, new Set());
}

module.exports.getExistedIcons = getExistedIcons;
