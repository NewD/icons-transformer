const path = require('path');
const fs = require('fs');
const { getIndexFileContent } = require('./filesFormatters/getIndexFileContentHelper');
const { getExistedIcons } = require('./cashHelpers/getExistedIconsHelper');

function updateIndexFile() {
    const iconsContainerPath = path.resolve('', process.env.ICONS_DIR_PATH);
    const existedIcons = getExistedIcons();

    // if have no iconName - get existed icons' files' names and write it in index file
    const fileContent = getIndexFileContent(Array.from(existedIcons.values()));

    const indexFile = fs.openSync(`${iconsContainerPath}/index.js`, 'w');
    fs.writeSync(indexFile, fileContent);
}

module.exports.updateIndexFile = updateIndexFile;
