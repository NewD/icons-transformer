const { GroupsIdsEnum } = require('../../enums/GroupsIdsEnum');
const { convertBigIcons } = require('./convertBigIconsHelper');
const { convertColored } = require('./convertColoredHelper');
const { convertSplitter } = require('./convertSplitterHelper');

function convertGroupToComponents(group) {
    switch (group.id) {
        case GroupsIdsEnum.splitter: return convertSplitter(group);

        case GroupsIdsEnum.colored: return convertColored(group);

        case GroupsIdsEnum.bigIcons: return convertBigIcons(group);

        default: return [];
    }
}

module.exports.convertGroupToComponents = convertGroupToComponents;
