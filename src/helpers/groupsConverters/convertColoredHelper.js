const { getFileNameByIconName } = require('../namesFormatters/getFileNameByIconNameHelper');

function convertColored(group) {
    return group.children.map(child => {
        const icon = child;

        icon.name = getFileNameByIconName(icon.name, true);

        return icon;
    });
}

module.exports.convertColored = convertColored;
