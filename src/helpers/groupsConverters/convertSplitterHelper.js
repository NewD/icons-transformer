function convertSplitter(group) {
    const splitterIds = {
        vertical: '12577:850',
        horizontal: '13581:392913'
    };

    return group.children.map(child => {
        const icon = child;

        if (icon.id === splitterIds.vertical) {
            icon.name = 'coloredVerticalSplitter';

            return icon;
        }

        icon.name = 'coloredHorizontalSplitter';

        return icon;
    });
}

module.exports.convertSplitter = convertSplitter;
