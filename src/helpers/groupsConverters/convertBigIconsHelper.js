function convertBigIcons(group) {
    const bigIconPrefix = 'IconsBigMW';
    const deleteIcon = 'deleteIcon';

    return group.children.map(child => {
        const icon = child;

        icon.name = `${icon.name.replace(
            bigIconPrefix,
            icon.name.includes(deleteIcon) ? '' : 'big'
        )}/nop`;

        return icon;
    });
}

module.exports.convertBigIcons = convertBigIcons;
