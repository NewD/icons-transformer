const { formatName } = require('./formatNameHelper');

function getFileNameByIconName(name, isColored = false) {
    let iconName = name;

    iconName = iconName.replace(/\s+$/, '');

    const componentName = iconName.split(/[+./_-\s]/);

    if (componentName.length === 1) {
        return formatName(componentName[0]);
    }

    const nameWithoutStyle = isColored
        ? componentName
        : componentName.slice(0, componentName.length - 1);

    const camelCaseName = nameWithoutStyle.reduce((acc, pathPart) => {
        return pathPart ? acc + pathPart[0].toUpperCase() + pathPart.slice(1) : acc;
    }, '');

    return formatName(camelCaseName);
}

module.exports.getFileNameByIconName = getFileNameByIconName;
