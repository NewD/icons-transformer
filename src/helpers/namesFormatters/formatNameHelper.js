function formatName(name) {
    return name.replace(name[0], name[0].toLowerCase());
}

module.exports.formatName = formatName;
