const path = require('path');

function getFilePath(
    fileName,
    directory = path.resolve('', process.env.ICONS_DIR_PATH),
    extension = process.env.ICON_EXTENSION
) {
    return `${directory}/${fileName}${extension}`;
}

module.exports.getFilePath = getFilePath;
