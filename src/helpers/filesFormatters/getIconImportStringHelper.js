function getIconImportString(iconName) {
    return `import ${iconName} from './${iconName + process.env.ICON_EXTENSION}';\n`;
}

module.exports.getIconImportString = getIconImportString;
