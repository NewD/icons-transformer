const { getFilePath } = require('../namesFormatters/getFilePathHelper');

const fs = require('fs');

function deleteExtraFiles(fileNames) {
    fileNames.forEach(fName => {
        fs.unlinkSync(getFilePath(fName));
    });
}

module.exports.deleteExtraFiles = deleteExtraFiles;
