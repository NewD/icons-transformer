function convertSvgToDynamicFillSvg(svgString, iconName, isColoredIcon) {
    const filteredSvg = svgString.replace(/clip0/gm, iconName);

    if (isColoredIcon) {
        return filteredSvg;
    }

    const fillFilter = /fill=".*?"/gm;
    const strokeFilter = /stroke=".*?"/gm;

    return filteredSvg
        .replace(fillFilter, 'fill="currentColor"')
        .replace(strokeFilter, 'stroke="currentColor"');
}

module.exports.convertSvgToDynamicFillSvg = convertSvgToDynamicFillSvg;
