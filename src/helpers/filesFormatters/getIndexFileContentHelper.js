const { getIconImportString } = require('./getIconImportStringHelper');

function getIndexFileContent(iconsFilesNames) {
    const fourSpaces = '    ';
    const defaultIconExport = iconsFilesNames.includes('okGray')
        ? `${fourSpaces}defaultIcon: okGray,\n`
        : '';

    return `${iconsFilesNames
        .map(name => {
            return getIconImportString(name);
        })
        .join('')}\nexport default {\n${
        defaultIconExport + fourSpaces + iconsFilesNames.join(`,\n${fourSpaces}`)
    }\n};\n`;
}

module.exports.getIndexFileContent = getIndexFileContent;
