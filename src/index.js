require('dotenv').config();

const isFullUpdate = process.argv[2] ? process.argv[2].toLowerCase() === '-a' : false;
const { fetchIcons } = require('./services/fetchIconsService');

fetchIcons(isFullUpdate);
