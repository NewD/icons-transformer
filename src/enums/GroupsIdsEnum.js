const GroupsIdsEnum = {
    colored: '13563:392597',
    splitter: '13581:392914',
    bigIcons: '13574:392398'
};

module.exports.GroupsIdsEnum = GroupsIdsEnum;
