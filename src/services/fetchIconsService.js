const { Api } = require('figma-api');
const { FramesIdsEnum } = require('../enums/FramesIdsEnum');
const { NodeTypesEnum } = require('../enums/NodeTypesEnum');
const { getExistedIcons } = require('../helpers/cashHelpers/getExistedIconsHelper');
const { formatName } = require('../helpers/namesFormatters/formatNameHelper');
const {
    getFileNameByIconName
} = require('../helpers/namesFormatters/getFileNameByIconNameHelper');
const {
    convertGroupToComponents
} = require('../helpers/groupsConverters/convertGroupToComponentsHelper');
const { updateIndexFile } = require('../helpers/updateIndexFileHelper');
const { downloadIcons } = require('../helpers/downloadHelpers/downloadIconsHelper');
const { coloredPrefix } = require('../constants');
const { deleteExtraFiles } = require('../helpers/filesFormatters/deleteExtraFilesHelper');

async function fetchIcons(isFullUpdate) {
    // usefull constants
    const fileKey = process.env.ICONS_FILE_KEY;
    const iconsRootNodeId = process.env.ICONS_NODES_ID;

    // figma api
    const api = new Api({
        personalAccessToken: process.env.PERSONAL_ACCESS_TOKEN
    });

    console.log('Загрузка дерева иконок...');
    // figma file with icons
    const iconsFile = await api.getFileNodes(fileKey, [iconsRootNodeId]);
    // icons' node from figma file
    const iconsNodes = iconsFile.nodes[iconsRootNodeId].document.children
        .filter(item => Object.values(FramesIdsEnum).includes(item.id))
        .reduce((acc, frame) => acc.concat(...frame.children), []);

    // icons and groups (filtered nodes)
    const icons = iconsNodes.reduce((acc, item) => {
        if (item.type === NodeTypesEnum.component) {
            acc.push(item);
        }

        if (item.type === NodeTypesEnum.group) {
            // add converted icons from groups
            acc.push(...convertGroupToComponents(item));
        }

        return acc;
    }, []);

    const extraIcons = getExistedIcons();
    const existedIcons = isFullUpdate ? new Set() : new Set(extraIcons);

    // convert icons to {ids: new Map(), names: new Map()} format
    const idsNames = icons.reduce(
        (acc, el) => {
            const fileName = getFileNameByIconName(el.name);

            // for correct processing of icons with 'colored' prefix
            const coloredFileName = fileName.startsWith(coloredPrefix)
                ? formatName(fileName.split(coloredPrefix)[1])
                : fileName;

            if (extraIcons.has(coloredFileName)) {
                extraIcons.delete(coloredFileName);
            }

            if (existedIcons.has(coloredFileName)) {
                return acc;
            }

            existedIcons.add(coloredFileName);

            return {
                ids: acc.ids.set(fileName, el.id),
                names: acc.names.set(el.id, fileName)
            };
        },
        { ids: new Map(), names: new Map() }
    );

    deleteExtraFiles(extraIcons);

    if (idsNames.ids.size === 0) {
        updateIndexFile();

        return;
    }

    const ids = Array.from(idsNames.ids.values());

    console.log('Получение ссылок на недостающие иконки...');
    // fetch icons' links by ids
    const { images } = await api.getImage(process.env.ICONS_FILE_KEY, {
        ids,
        format: 'svg'
    });

    const iconsInfo = Object.entries(images);

    // download SVGs by links and create files
    const downloadingPromises = downloadIcons(iconsInfo, idsNames);

    console.log('Скачивание новых иконок...');
    await Promise.all(downloadingPromises).finally(updateIndexFile);

    const names = Array.from(idsNames.names.values());

    console.log(`Количество добавленых иконок ${names.length}:\n`, names);
}

module.exports.fetchIcons = fetchIcons;
